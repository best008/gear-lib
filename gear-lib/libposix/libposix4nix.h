/******************************************************************************
 * Copyright (C) 2014-2020 Zhifeng Gong <gozfree@163.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
#ifndef LIBPOSIX4NIX_H
#define LIBPOSIX4NIX_H

#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include <sys/uio.h>

#include "kernel_list.h"


#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
 * basic types
 ******************************************************************************/

/******************************************************************************
 * I/O string APIs
 ******************************************************************************/

/******************************************************************************
 * sys file APIs
 ******************************************************************************/

/******************************************************************************
 * pthread APIs
 ******************************************************************************/

/******************************************************************************
 * time APIs
 ******************************************************************************/

/******************************************************************************
 * driver IOC APIs
 ******************************************************************************/

/******************************************************************************
 * socket APIs
 ******************************************************************************/

/******************************************************************************
 * system APIs
 ******************************************************************************/
#define pipe_read read
#define pipe_write write

/******************************************************************************
 * memory APIs
 ******************************************************************************/

/*
 * simple reflection c version realization
 */
typedef void (*void_fn)(void);
struct reflect {
    void_fn fn;
    const char* name;
};
struct reflect __start_reflect;
struct reflect __stop_reflect;

#define REFLECT_DEF(x) __attribute__((section("reflect"), aligned(sizeof(void*)))) \
            struct reflect __##x = {(void_fn)x, #x};

#define REFLECT_CALL(type, x, ...)                                            \
    do {                                                                      \
        struct reflect *_r = NULL;                                            \
        for (_r = &__start_reflect; _r < &__stop_reflect; _r++) {             \
            if (strcmp(_r->name, x) == 0) {                                   \
                type __fn = (type)_r->fn;                                     \
                __fn(__VA_ARGS__);                                            \
            }                                                                 \
        }                                                                     \
    } while (0)


#ifdef __cplusplus
}
#endif
#endif
